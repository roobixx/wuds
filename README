# WUDS: Wi-Fi User Detection System

WUDS is a proximity detection system that uses Wi-Fi probe requests, signal strength, and a white list of MAC addresses to create a detection barrier and identify the presence of foreign devices within a protected zone. Designed with the Raspberry Pi in mind, WUDS can be installed and configured on any system with Python 2.x and a wireless card capable of Monitor mode. See [http://www.lanmaster53.com/2014/10/wifi-user-detection-system/](http://www.lanmaster53.com/2014/10/wifi-user-detection-system/) for more information.

## Setup

```bash
# iw is needed to control the wi-fi interface
sudo apt-get install iw
# (optional) I use screen as a daemon for WUDS
sudo apt-get install screen
screen
# install WUDS
git clone https://LaNMaSteR53@bitbucket.org/LaNMaSteR53/wuds.git
cd wuds
# edit the config file
vim config.py
# execute the included run script
./run.sh
# Ctrl+A, D detaches from the screen session
```

## File Summary

* alerts.py -- custom alert modules
* config.py -- configuration file
* core.py -- core library
* run.sh -- startup script
* README -- this file